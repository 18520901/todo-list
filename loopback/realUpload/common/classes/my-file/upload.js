var multer = require('multer');
var path = require('path');

var uploadedFile = '';

const storage = multer.diskStorage({
  destination: 'C:/Users/admin/Desktop/nodejs/webdev/loopback/realUpload/public/uploads',
  filename: function (req, file, cb) {
    uploadedFile = file.originalname;
    cb(null, file.originalname);
  }
});
var upload = multer({ storage: storage }).single('upload');
module.exports = Myfile => {
  Myfile.prototype.upload = (req, res) => {
    return new Promise((resolve, reject) => {
      try {
        upload(req, res, function (err) {
          if (err) {
            // show error khi uploading
            res.json(err)
          }
          resolve(uploadedFile);
        });
      } catch (error) {
        reject(error);
      }
    })
  }
}