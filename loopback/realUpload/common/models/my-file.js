'use strict';



module.exports = function (Myfile) {
    require('../classes/my-file')(Myfile);
  
    Myfile.upload = async (req, res, cb) => {
      try {
        let myfile = new Myfile();
        return await myfile.upload(req, res);
      } catch (err) {
        cb(err);
      }
    }
  
    Myfile.remoteMethod('upload', {
      http: { path: '/upload', verb: 'post' },
      accepts: [{
        arg: 'req',
        type: 'object',
        http: {
          source: 'req'
        }
      }, {
        arg: 'res',
        type: 'object',
        http: {
          source: 'res'
        }
      }],
      returns: {
        arg: 'data',
        type: 'string'
      },
    });
  };
