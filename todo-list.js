var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var mongodb = require('mongodb');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/myapp', { useNewUrlParser: true });
var Schema = mongoose.Schema;
app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({ extended: true }));


var userSchema = new Schema({
    name: { type: String, required: true },
    tasks: [{
        type: Schema.Types.ObjectId,
        ref: 'Todo',
    }]
})

var todoSchema = new Schema({
    content: String
})

const User = mongoose.model('User', userSchema);
const Todo = mongoose.model('Todo', todoSchema);

//create
app.post('/create', async (req, res, next) => {
    let usr = await User.create({ name: "//insert name" });//
    let task = await Todo.create({ content: "insert task" });//
    let result = await usr.tasks.push(task);
    await usr.save();
    console.log(usr.populate(Todo))
})

//read
app.post('/read', async (req, res, next) => {
    let temp = await User.find();
    for (let i = 0; i < temp.length; i++) {
        await console.log(temp[i].populate(Todo));
    }
})

//update
app.post('/update', async (req, res, next) => {
    let Task = await Todo.find();
    await console.log(Task)
    todo.findOneAndUpdate({ "_id": "//insert id" }, { content: "editeddd" }, function (err, result) {
        if (err) throw err
    });
})

//delete
app.post('/delete', async (req, res, next) => {
    let deltask = await Todo.find();
    await console.log(deltask)
    todo.findByIdAndDelete({ "_id": "//insert id" }, function (err, result) {
        if (err) throw err
    })
})

app.listen(8080);